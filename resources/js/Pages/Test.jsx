import React, { useState } from 'react';

const Test = ({ fname, lname }) => {
    return (
        <>
            <h1>This is test component</h1>
            <h2>{fname}</h2>
            <h2>{lname}</h2>
        </>
    )
}

export default Test