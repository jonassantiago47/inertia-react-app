<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Laravel routing ~ this will render the welcome.blade.php that we deleted
Route::get('/welcome', function () {
    return view('welcome'); 
    
});

Route::get('/', function () {
    return Inertia::render('Test', [
        'fname' => 'Enter your First Name',
        'lname' => 'Enter your Last Name',
    ]);
});


